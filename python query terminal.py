import mysql.connector
from datetime import date, datetime, timedelta
from mysql.connector import errorcode
cnx = None

try:
    cnx = mysql.connector.connect(user = 'root', host = '127.0.0.1')
except mysql.connector.Error as err:
    if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
        print("Something is wrong with your user name or password")
    elif err.errno == errorcode.ER_BAD_DB_ERROR:
        print("Database does not exist")
    else :
        print("Failed connect database server : {}".format(err))
else :
    print('connected : {}'.format(cnx))
    cursor = cnx.cursor(dictionary=True)


if(cnx != None):
        while True:
            try:
                print('>> ', end = '')
                query = input()
                for result in cursor.execute(query, multi=True):
                    if result.with_rows:
                        for row in result.fetchall():
                            print(row)
                    else:
                        print("Number of affected rows: {}".format(result.rowcount))
                
            except mysql.connector.Error as err:
                print("Operation failed : {}".format(err))
                continue
